db.users.insertMany([
	{
		firstName: "Diane",
		lastName: "Murphy",
		email: "dmurphy@gmail.com",
		isAdmin: false,
		isActive: true
	},
	{
		firstName: "Mary",
		lastName: "Patterson",
		email: "mpatterson@gmail.com"
		isAdmin: false,
		isActive: true
	},
	{
		firstName: "Jeff",
		lastName: "Firrelli",
		email: "jefffirrelli@gmail.com",
		isAdmin: false,
		isActive: true
	},
	{
		firstName: "Gerald",
		lastName: "Bondur",
		email: "geralbondur@gmail.com",
		isAdmin: false,
		isActive: true
	},
	{
		firstName: "Pamela",
		lastName: "Castillo",
		email: "pamelacastillo@gmail.com",
		isAdmin: true,
		isActive: false
	},
	{
		firstName: "George",
		lastName: "Vanauf",
		email: "gvanauf@mail.com",
		isAdmin: true,
		isActive: true
	},
	]);

	db.courses.insertMany([
		{
			name: "Professional Development",
			price: 10000
		},
		{
			name: "Business Processing",
			price: 13000,
		}
	]);




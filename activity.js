db.users.insertMany([
{
	name: "Stephen",
	lastName: "Hawking",
	age: 76,
	email: "Stephenhawking@mail.com",
	department: "HR"
},
{
	name: "Neil",
	lastName: "Armstong",
	age: 82,
	email: "neilarmstrong@mail.com",
	department: "HR"
},
{
	name: "Bill",
	lastName: "Gates",
	age: 65,
	email: "billgates@mail.com",
	department: "Operations"
},
{
	name: "Jane",
	lastName: "Doe",
	age: 21,
	email: "janedoe@mail.com",
	department: "HR"
}
])


db.users.find(
		{
			$or: [
				{firstName: "Neil"},
				{lastName: "Armstong" },
				{_id: 0, firstName: 1, lastName: 1}
			]
		});



db.users.find({
	$and: [
		{ 
			age: { $gte: 70}
		},
		{
			department: "HR"
		}
	]
});



db.users.find({
	$and: [
		{
			name:  {$regex: 'E', $options: 'i'}
		},
		{ 
			age: { $lte: 30}
		}
	]
});
